from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from . models import Branches
from . serializers import bankSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import LimitOffsetPagination

#ifsc based search
@api_view(['GET'])
def branchDetail(request):
    try:
        bankObj=Branches.objects.get(ifsc=request.GET.get('ifsc'))
        serializer=bankSerializer(bankObj)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({
            'message': "ifsc doesn't exist"
        }, status=status.HTTP_404_NOT_FOUND)

#for limits and paginations
class LimitOffset(LimitOffsetPagination):
    #overriding the rest_framework limit and offset
    default_limit = 100
    limit_query_param='limit'
    offset_query_param='offset'
    max_limit=100

# for autocomplete on any fields
class AutoComplete(APIView):
    def post(self):
        pass
    def get(self,request):
        try:
            branch1 = Branches.objects.all()
            ifsc_q = self.request.query_params.get('ifsc','')
            bankid_q = self.request.query_params.get('bank_id','')
            branch_q = self.request.query_params.get('branches','')
            addr_q= self.request.query_params.get('address','')
            city_q = self.request.query_params.get('city','')
            district_q = self.request.query_params.get('district','')
            state_q = self.request.query_params.get('state','')
            autocomplete_q = self.request.query_params.get('autocomplete','')
            serializer = bankSerializer(branch1,many=True)

            #checks for all rows and columns
            if city_q:
                serializer = bankSerializer(branch1.filter(city__icontains=city_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            if ifsc_q:
                serializer = bankSerializer(branch1.filter(ifsc__icontains=ifsc_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            if bankid_q:
                serializer = bankSerializer(branch1.filter(bank_id__icontains=bankid_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            if branch_q:
                serializer = bankSerializer(branch1.filter(branch__icontains=branch_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            if addr_q:
                serializer = bankSerializer(branch1.filter(address__icontains=addr_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            if district_q:
                serializer = bankSerializer(branch1.filter(district__icontains=district_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            if state_q:
                serializer = bankSerializer(branch1.filter(state__icontains=state_q),many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)



            #autocomplete
            if autocomplete_q:
                serializer = bankSerializer(branch1.filter(city__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                serializer = bankSerializer(branch1.filter(ifsc__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                serializer = bankSerializer(branch1.filter(bank_id__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                serializer = bankSerializer(branch1.filter(branch__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                serializer = bankSerializer(branch1.filter(address__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                serializer = bankSerializer(branch1.filter(district__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                serializer = bankSerializer(branch1.filter(state__icontains=autocomplete_q),many=True)
                if serializer.data!=[]:
                    return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as exception:
            return Response({
                'message':"Record doesn't exist"
            }, status=status.HTTP_404_NOT_FOUND)



#filter on multiple fields
class bankList(ListAPIView):
    queryset=Branches.objects.all()
    serializer_class = bankSerializer
    filter_backends = [DjangoFilterBackend]

    #all fields applied filters
    filterset_fields = ['ifsc','bank_id','branch','address','city','district','state','bank_name']

    #limit and offset
    pagination_class = LimitOffset
    